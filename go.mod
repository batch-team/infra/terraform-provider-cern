module gitlab.cern.ch/batch-team/infra/terraform-provider-cern

go 1.13

require (
	github.com/go-ldap/ldap v3.1.2+incompatible
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.10.1
	gitlab.cern.ch/batch-team/negotiate v1.1.3
)
